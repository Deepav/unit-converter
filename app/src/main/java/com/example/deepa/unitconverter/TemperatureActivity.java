package com.example.deepa.unitconverter;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
/**
 * class TemperatureActivity is class called by onClick method of MainActivity class upon user clicks button TEMPERATURE
 *  It implements conversion from meter to Celsius and Fahrenheit to Fahrenheit to Celsius.
 * This class implements listener  OnClickListener for temButton.
 * It has EditText field in for taking user input for volume unit conversion.
 * and EditText field out for displaying the output to the user configured in activity_temperature.xml
 * r1, r2 are RadioButtons to be selected for desired unit of conversion.
 * This class intent file is activity_temperature.xml
 */
public class TemperatureActivity extends AppCompatActivity {

    private EditText in;
    private RadioButton r1;
    private RadioButton r2;
    private  EditText out;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temperature);
        /*Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
        onSelect();
    }
    /**
     * onSelect() is a void method to do unit conversion.
     * This method gets user input value by in.getText() method.
     * Upon selecting one of the radio button , other radio button is set to false.
     * 'n' is a Double value to store user input in numerical form .
     * 'result' is a Double value to hold the converted value upon mathematical conversion.
     * Later Output field will display the value of 'result' variable using out.setText()
     */
    public void onSelect()
    {
       in=(EditText)findViewById(R.id.editTextInput);
        r1=(RadioButton)findViewById(R.id.radioButtonC);
        r2=(RadioButton)findViewById(R.id.radioButtonFt);
        out=(EditText)findViewById(R.id.editTextOutput);

        r1.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v)
            {
                //If user selects the output to be in Celsius
                r2.setChecked(false);
                Float n=Float.valueOf(in.getText().toString());
                Float result=(n-32)*5/9;
                out.setText(Float.toString(result)+" C");
                //in.setText(Double.toString(n)+" F");
            }
        });

        r2.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v)
            {
                //If user selects the output to be in Fahrenheit
                r1.setChecked(false);
                Float n=Float.valueOf(in.getText().toString());
                Float result=(n*9/5)+32;
                out.setText(Float.toString(result)+" F");
                //in.setText(Double.toString(n)+" C");
            }
        });
    }

}
