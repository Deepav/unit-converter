package com.example.deepa.unitconverter;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
/**
 * class LengthActivity is class called by onClick method of MainActivity class upon user clicks button LENGTH
 *  It implements conversion from meter to feet and feet to meter.
 * This class implements listener  OnClickListener for lenButton.
 * It has EditText field in for taking user input for volume unit conversion
 * and EditText field out for displaying the output to the user configured in activity_length.xml
 * r1, r2 are RadioButtons to be selected for desired unit of conversion.
 * This class intent file is activity_length.xml
 */
public class LengthActivity extends AppCompatActivity {

   private EditText in;
   private RadioButton r1;
   private RadioButton r2;
   private  EditText out;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_length);
        /*Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
        onSelect();
    }

    /**
     * onSelect() is a void method to do unit conversion.
     * This method gets user input value by in.getText() method.
     * Upon selecting one of the radio button , other radio button is set to false.
     * 'n' is a Double value to store user input in numerical form .
     * 'result' is a Double value to hold the converted value upon mathematical conversion.
     * Later Output field will display the value of 'result' variable using out.setText()
     */
    public void onSelect()
    {
        in=(EditText)findViewById(R.id.editTextInput);
        r1=(RadioButton)findViewById(R.id.radioButtonMt);
        r2=(RadioButton)findViewById(R.id.radioButtonFt);
        out=(EditText)findViewById(R.id.editTextOutput);

        r1.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v)
            {
                //If user selects the output to be in meter.
                r2.setChecked(false);
                Double n =Double.valueOf(in.getText().toString());
                Double result = n*0.3048;
                out.setText(Double.toString(result) + " m");
                //in.setText(Double.toString(n)+" F");
            }
        });

        r2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //If user selects the output to be in feet
                r1.setChecked(false);
                Double n=Double.valueOf(in.getText().toString());
                Double result=n*3.2808399;
                out.setText(Double.toString(result) + " ft");
                //in.setText(Double.toString(n)+" C");
            }
        });
    }
}
