package com.example.deepa.unitconverter;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
/**
 * class VolumeActivity is class called by onClick method of MainActivity class upon user clicks button VOLUME
 *  It implements conversion from Liter to Gallon and Gallon to Liter
 * This class implements listener  OnClickListener for temButton.
 * It has EditText field in for taking user input for volume unit conversion
 * and EditText field out for displaying the output to the user configured in activity_volume.xml
 * r1, r2 are RadioButtons to be selected for desired unit of conversion.
 * This class intent file is activity_volume.xml
 */
public class VolumeActivity extends AppCompatActivity {

    private EditText in;
    private RadioButton r1;
    private RadioButton r2;
    private  EditText out;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_volume);
        /*Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
        onSelect();
    }
    /**
     * onSelect() is a void method to do unit conversion.
     * This method gets user input value by in.getText() method.
     * Upon selecting one of the radio button , other radio button is set to false.
     * 'n' is a Double value to store user input in numerical form .
     * 'result' is a Double value to hold the converted value upon mathematical conversion.
     * Later Output field will display the value of 'result' variable using out.setText()
     */
    public void onSelect()
    {

        in=(EditText)findViewById(R.id.editTextInput);
        r1=(RadioButton)findViewById(R.id.radioButtonLt);
        r2=(RadioButton)findViewById(R.id.radioButtonGa);
        out=(EditText)findViewById(R.id.editTextOutput);

        r1.setOnClickListener(new View.OnClickListener() {
            //If user selects the output to be in Liter
            public void onClick(View v) {
                r2.setChecked(false);
                Double n = Double.valueOf(in.getText().toString());
                Double result = n*3.785411784;
                out.setText(Double.toString(result) + " L");
                //in.setText(Double.toString(n)+" F");
            }
        });

        r2.setOnClickListener(new View.OnClickListener() {
            //If user selects the output to be in Gallon
            public void onClick(View v) {
                r1.setChecked(false);
                Double n = Double.valueOf(in.getText().toString());
                Double result = n*0.264172052358;
                out.setText(Double.toString(result) + " gal");
                //in.setText(Double.toString(n)+" C");
            }
        });
    }


}
