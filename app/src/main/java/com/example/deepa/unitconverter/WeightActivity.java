package com.example.deepa.unitconverter;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
/**
 * class WeightActivity is class called by onClick method of MainActivity class upon user clicks button WEIGHT
 *  It implements conversion from kilogram to pound and pound to kilogram
 * This class implements listener  OnClickListener for weighButton.
 * It has EditText field in for taking user input for volume unit conversion
 * and EditText field out for displaying the output to the user configured in activity_weight.xml
 * r1, r2 are RadioButtons to be selected for desired unit of conversion.
 * This class intent file is activity_weight.xml
 */
public class WeightActivity extends AppCompatActivity {

    private EditText in;
    private RadioButton r1;
    private RadioButton r2;
    private  EditText out;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weight);
        /*Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
        onSelect();
    }
    /**
     * onSelect() is a void method to do unit conversion.
     * This method gets user input value by in.getText() method.
     * Upon selecting one of the radio button , other radio button is set to false.
     * 'n' is a Double value to store user input in numerical form .
     * 'result' is a Double value to hold the converted value upon mathematical conversion.
     * Later Output field will display the value of 'result' variable using out.setText()
     */
    public void onSelect()
    {
        in=(EditText)findViewById(R.id.editTextInput);
        r1=(RadioButton)findViewById(R.id.radioButtonKg);
        r2=(RadioButton)findViewById(R.id.radioButtonLb);
        out=(EditText)findViewById(R.id.editTextOutput);

        r1.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v)
            {
                //If user selects the output to be in kilogram
                r2.setChecked(false);
                Double n =Double.valueOf(in.getText().toString());
                Double result = n/2.2046;
                out.setText(Double.toString(result) + " kg");
                //in.setText(Double.toString(n)+" F");
            }
        });

        r2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //If user selects the output to be in Gallon
                r1.setChecked(false);
                Double n = Double.valueOf(in.getText().toString());
                Double result = n * 2.2046;
                out.setText(Double.toString(result) + " lb");
                //in.setText(Double.toString(n)+" C");
            }
        });
    }

}
