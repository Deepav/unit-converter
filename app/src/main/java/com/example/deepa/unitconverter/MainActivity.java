package com.example.deepa.unitconverter;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
/**
 * Project Name:Unit Converter System: This is an app for unit conversion system for Temperature(Celsius - Fahrenheit), Length(Meter-Feet),Weight(kg-Pound),Volume(Liter-Gallon)
 * @author Deepa Veerabhadrachar
 * Group members:Anthony Filippo, Deepa Veerabhadrachar, Do Phan, Meena kumari Mudduluru
 * Date:30th-Nov-2015
 * @version version 1
 */

/**
 * This is the main java class to the point of entry to Unit Converter application.
 * Application main page has 4 buttons:TEMPERATURE,LENGTH,WEIGHT,VOLUME.
 * Each button leads to respective Intent calling respective java class TemperatureActivity,LengthActivity,WeightActivity,VolumeActivity.
 */

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        /**
         * onCreate((Bundle savedInstanceState) is a public void method similar to main()
         * It sets Content view to activity_main.xml
         * Here we call method to create 4 buttons
         */
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /*Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
        onclickTemperatureButton();
        onclickLengthButton();
        onclickWeightButton();
        onclickVolumeButton();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * onclickTemperatureButton() is a void method.
     * This method create Button temButton referring buttonTemperature of activity_temperature.xml
     * Then calls method onClick upon clicking temButton to set next intent to com.example.deepa.unitconverter.TemperatureActivity
     */
    public void onclickTemperatureButton()
    {
        Button temButton=(Button) findViewById(R.id.buttonTemperature);
        temButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent("com.example.deepa.unitconverter.TemperatureActivity"));
            }
        });
    }
    /**
     * onclickLengthButton() is a void method.
     * This method create Button lenButton referring buttonLength of activity_length.xml
     * Then calls method onClick upon clicking temButton to set next intent to com.example.deepa.unitconverter.LengthActivity
     */
    public void onclickLengthButton()
    {
        Button lenButton=(Button) findViewById(R.id.buttonLength);
        lenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent("com.example.deepa.unitconverter.LengthActivity"));
            }
        });
    }
    /**
     *onclickWeightButton() is a void method.
     * This method create Button weighButton referring buttonWeight of activity_weight.xml
     * Then calls method onClick upon clicking temButton to set next intent to com.example.deepa.unitconverter.WeightActivity
     */
    public void onclickWeightButton()
    {
        Button weighButton=(Button) findViewById(R.id.buttonWeight);
        weighButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent("com.example.deepa.unitconverter.WeightActivity"));
            }
        });
    }
    /**
     * onclickVolumeButton() is a void method.
     * This method create Button volButton referring buttonWeight of activity_volume.xml
     * Then calls method onClick upon clicking temButton to set next intent to com.example.deepa.unitconverter.VolumeActivity
     */
    public void onclickVolumeButton()
    {
        Button volButton=(Button) findViewById(R.id.buttonVolume);
        volButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent("com.example.deepa.unitconverter.VolumeActivity"));
            }
        });
    }
}
